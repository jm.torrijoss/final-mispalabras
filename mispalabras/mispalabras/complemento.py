from .models import Word, Comment, Voto, Usuario, Info, Enlace
from .forms import ContentForm, TextForm
from urllib.error import HTTPError
from xml.dom.minidom import parse
import urllib.request
from urllib.parse import quote
from html.parser import HTMLParser
from django.utils import timezone

def gest_comentario(request, word):
    if 'comment' in request.POST:
        p = Word.objects.get(word=word)
        form = ContentForm(request.POST)
        if form.is_valid():
            c =Comment(comentario=form.cleaned_data['content'])
            c.palabra = p
            c.usuario = Usuario.objects.get(usuario=request.user)
            c.date = timezone.now()
            c.save()

class ParserDefi(HTMLParser):
    def __init__(self):
        super().__init__()
        self.defi = ''
        self.atr =[]
        self.select = False
        self.val = ''
        self.sel = False
    def handle_starttag(self, tag, attrs):
        if tag == 'meta':
            self.atr.append(('Final','Inicio'))
            for atr in attrs:
                self.atr.append(atr)
            for at in self.atr:
                if at[0] == 'Final' and at[1] == 'Inicio':
                    self.val = ''
                    self.select = False
                    self.sel = False
                if self.select:
                    if at[0] == 'content':
                        self.defi = at[1]
                        self.select = False
                        break
                if at[0] == 'property' and at[1] == 'og:description':
                    self.select = True
                    if self.sel:
                        self.defi = self.val
                        break
                if at[0] == 'content':
                    self.sel = True
                    self.val = at[1]


class ParserImg(HTMLParser):
    def __init__(self):
        super().__init__()
        self.img = ''
        self.atr =[]
        self.sel_img = False
        self.val = ''
        self.sel = False
    def handle_starttag(self, tag, attrs):
        if tag == 'meta':
            self.atr.append(('Final', 'Inicio'))
            for atr in attrs:
                self.atr.append(atr)
            for at in self.atr:
                if at[0] == 'Final' and at[1] == 'Inicio':
                    self.val = ''
                    self.sel_img = False
                    self.sel = False
                if self.sel_img:
                    if at[0] == 'content':
                        self.img = at[1]
                        self.sel_img = False
                        break
                if at[0] == 'property' and at[1] == 'og:image':
                    self.sel_img = True
                    if self.sel:
                        self.img = self.val
                        break
                if at[0] == 'content':
                    self.sel = True
                    self.val = at[1]


def rae(request,word):
    if request.method == 'POST' and 'rae' in request.POST:
        url = 'https://dle.rae.es/'+str(quote(word))
        user_agent = "Mozilla/5.0 (X11) Gecko/20100101 Firefox/100.0"
        headers = {'User-Agent': user_agent}
        req = urllib.request.Request(url, headers=headers)
        parserdefi = ParserDefi()
        parserimg = ParserImg()
        with urllib.request.urlopen(req) as response:
            html = response.read().decode('utf8')
            parserdefi.feed(html)
            parserimg.feed(html)
            definicion = [parserdefi.defi,parserimg.img]
        r = Info(web='rae')
        r.defi = definicion[0]
        r.palabra = Word.objects.get(word=word)
        r.img = definicion[1]
        r.usuario = Usuario.objects.get(usuario=request.user)
        r.date = timezone.now()
        r.save()
    else:
        definicion = [None,None]
    return definicion



def flickr(request,word):
    if request.method == 'POST' and 'flickr' in request.POST:
        try:
            url = "https://www.flickr.com/services/feeds/photos_public.gne?tags=" + str(quote(word))
            xml = parse(urllib.request.urlopen(url))
            feed = xml.getElementsByTagName('feed')
            entry = feed[0].getElementsByTagName('entry')
            link = entry[0].getElementsByTagName('link')
            for tag in link:
                atributo = tag.getAttribute('rel')
                if atributo == 'enclosure':
                    href = tag.getAttribute('href')

        except AttributeError:
            href = ''
        except IndexError:
            href = ''
        i = Info(web='flickr')
        i.img = href
        i.usuario = Usuario.objects.get(usuario=request.user)
        i.palabra = Word.objects.get(word=word)
        i.date = timezone.now()
        i.save()
    else:
        try:
            href = Info.objects.get(web='flickr',palabra=word).img
        except Info.DoesNotExist:
            href = None
    return href



def apimeme(request,word):
    if request.method == 'POST' and 'apimeme' in request.POST:
        form = TextForm(request.POST)
        if form.is_valid():
            texto = form.cleaned_data['text']
            option = form.cleaned_data['option']

            if option == '1':
                img = "http://apimeme.com/meme?meme=Error-404&top="+ str(quote(word))+"&bottom=" + str(quote(texto))
            elif option == '2':
                img = "http://apimeme.com/meme?meme=Fim-De-Semana&top="+ str(quote(word))+"&bottom=" + str(quote(texto))
            elif option == '3':
                img = "http://apimeme.com/meme?meme=Surpised-Frodo&top="+ str(quote(word))+"&bottom=" + str(quote(texto))

        i = Info(web='apimeme')
        i.img = img
        i.usuario = Usuario.objects.get(usuario=request.user)
        i.palabra = Word.objects.get(word=word)
        i.date = timezone.now()
        i.save()
    else:
        try:
            img = Info.objects.get(web='apimeme',palabra=word).img
        except Info.DoesNotExist:
            img = None
    return img



def gest_enlace(request,word):
    if request.method == 'POST' and 'btnenlace' in request.POST:
        try:
            form = request.POST['enlace']
            user_agent = "Mozilla/5.0 (X11) Gecko/20100101 Firefox/100.0"
            headers = {'User-Agent': user_agent}
            req = urllib.request.Request(form, headers=headers)
            response = urllib.request.urlopen(req)
            html = response.read().decode('utf8')
            parserdefi = ParserDefi()
            parserimg = ParserImg()
            parserdefi.feed(html)
            parserimg.feed(html)
            definicion = [parserdefi.defi, parserimg.img]
            r = Enlace(url=form)
            r.defi = definicion[0]
            r.palabra = Word.objects.get(word=word)
            r.img = definicion[1]
            r.usuario = Usuario.objects.get(usuario=request.user)
            r.date = timezone.now()
            r.save()
        except UnicodeError:
            r = Enlace(url=form)
            r.defi = ''
            r.palabra = Word.objects.get(word=word)
            r.img = ''
            r.usuario = Usuario.objects.get(usuario=request.user)
            r.date = timezone.now()
            r.save()
        except HTTPError:
            form = request.POST['enlace']
            response = urllib.request.urlopen(form)
            html = response.read().decode('utf8')
            parserdefi = ParserDefi()
            parserimg = ParserImg()
            parserdefi.feed(html)
            parserimg.feed(html)
            definicion = [parserdefi.defi, parserimg.img]
            r = Enlace(url=form)
            r.defi = definicion[0]
            r.palabra = Word.objects.get(word=word)
            r.img = definicion[1]
            r.usuario = Usuario.objects.get(usuario=request.user)
            r.date = timezone.now()
            r.save()
    try:
        info = Enlace.objects.filter(palabra=word)
    except Enlace.DoesNotExist:
        info = None
    return info

