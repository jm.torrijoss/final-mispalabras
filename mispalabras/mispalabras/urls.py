from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('login/', views.acceder, name='login'),
    path('logout/', views.user_logout, name='logout'),
    path('registro/', views.registro, name='registro'),
    path('mipagina/', views.mipagina, name='mipagina'),
    path('ayuda/', views.ayuda, name='ayuda'),
    path('<word>', views.word, name='palabra'),
]
