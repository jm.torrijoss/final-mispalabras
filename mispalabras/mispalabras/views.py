import random
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from django.core.paginator import Paginator
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth import logout, login, authenticate
from .models import Word, Comment, Voto, Usuario, Info, Enlace
from .forms import ContentForm, UserForm, TextForm, UrlForm
from . import complemento
from . import wikipedia
from django.utils import timezone

@csrf_exempt
def index(request):
    lista = Word.objects.all().order_by('-date')[0:5]
    lista_pal_voto = Word.objects.all().order_by('-contador')[0:10]
    try:
        palabra = [len(lista),random.choice(lista).word]
    except IndexError:
        palabra = [0,None]

    objeto_pag = Paginator(lista, 5).get_page(request.GET.get('page'))

    format_get =  'format' in request.GET
    if request.method == 'GET' and format_get:
        form = request.GET.get('format')
        if form == 'xml':
            response = render(request, 'pages/contenidos.xml', {'Lista': lista}, content_type='text/xml')
        if form == 'json':
            response = render(request, 'pages/contenidos.json', {'Lista': lista}, content_type='text/json')
        return response

    if request.method == 'GET' or 'POST':
        authform = AuthenticationForm()
        form = request.POST.get('content')
        if form is not None:
            return HttpResponseRedirect('/'+str(form))
        else:
            response = render(request, 'pages/word_form.html',
            {'Lista': lista,
            'login': authform,
            'word': '',
            'lista_pal_voto': lista_pal_voto,
            'numeropal': palabra[0],
            'palabra': palabra[1], 'objeto_pag': objeto_pag})
        return HttpResponse(response)

def word(request,word):
    word = word.lower()
    format_get =  'format' in request.GET
    try:
        if request.method == 'GET' and format_get:
            formato = request.GET.get('format')
            pala = Word.objects.get(word=word)
            try:
                coment = Comment.objects.filter(palabra=word)
            except Comment.DoesNotExist:
                com =''
            try:
                vot = Voto.objects.filter(palabra=word)
            except Voto.DoesNotExist:
                vot = ''
            try:
                inf = Info.objects.filter(palabra=word)
            except Info.DoesNotExist:
                inf=''
            try:
                enla = Enlace.objects.filter(palabra=word)
            except Enlace.DoesNotExist:
                enla =''
            if formato == 'xml':
                response = render(request, 'pages/word.xml', {'pal': pala, 'com':coment, 'vot':vot, 'inf': inf, 'enla': enla},
                                  content_type='text/xml')
            if formato == 'json':
                response = render(request, 'pages/word.json', {'pal': pala, 'com':coment, 'vot':vot, 'inf': inf, 'enla': enla},
                                  content_type='text/json')
            return response

        meth = request.method
        if meth == 'GET' or 'POST':
            try:
                p = Word.objects.get(word=word)
                defi = (p.defi,p.img)
                guarda = None
                complemento.gest_comentario(request, word)
            except Word.DoesNotExist:
                defi = wikipedia.wiki(request,word)
                if request.method == 'GET':
                    guarda = 'Guardar palabra'
                elif request.method == 'POST':
                    guarda = guardar_pal(request, word, defi)
            imgfli = complemento.flickr(request, word)
            comentarios = Comment.objects.filter(palabra=word)
            voto = votos(request, word)
            try:
                infrae = Info.objects.get(web='rae',palabra=word)
                drae = [infrae.defi,infrae.img]
            except Info.DoesNotExist:
                drae = complemento.rae(request,word)
            try:
                cuenta = Word.objects.get(word=word)
                veces = cuenta.contador
            except  Word.DoesNotExist:
                veces = 0
            comment = ContentForm()
            enlace = UrlForm()
            tarjeta = complemento.gest_enlace(request,word)
            lista = Word.objects.all().order_by('-date')[0:5]
            lista_pal_voto = Word.objects.all().order_by('-contador')[0:10]
            try:
                palabra = [len(lista),random.choice(lista).word]
            except IndexError:
                palabra = [0,None]
            authform = UserForm()
            imgapi = complemento.apimeme(request,word)
            texto = TextForm()
            content_re = render(request, 'pages/word.html',
                {'login': authform,
                'word': word, 'defi': defi[0],
                'imagen':defi[1],
                'voto': voto,
                'veces': veces,
                'guarda': guarda,
                'form': comment,
                'comentarios': comentarios,
                'drae': drae[0],
                'draeimg': drae[1],
                'numeropal': palabra[0],
                'palabra': palabra[1],
                'imgfli': imgfli,
                'texto':texto,
                'imgapi': imgapi,
                'enlace': enlace,
                'tarjeta': tarjeta,
                'lista_pal_voto': lista_pal_voto})
            response = HttpResponse(content_re)
    except TypeError:
        if request.method == 'GET' or request.method == 'POST':
            try:
                p = Word.objects.get(word=word)
                defi = (p.defi,p.img)
            except Word.DoesNotExist:
                defi = wikipedia.wiki(request,word)

            lista = Word.objects.all().order_by('-date')[0:10]
            lista_pal_voto = Word.objects.all().order_by('-contador')[0:10]
            try:
                palabra = [len(lista), random.choice(lista).word]
            except IndexError:
                palabra = [0, None]
            authform = UserForm()
            content_re = render(request, 'pages/word.html', {'word': word, 'defi': defi[0], 'imagen':defi[1],
                                                             'login': authform, 'numeropal': palabra[0],
                                                             'palabra': palabra[1], 'lista_pal_voto': lista_pal_voto})
            response = HttpResponse(content_re)
    return (response)
def votos(request, word):
    try:
        p = Word.objects.get(word=word)
        if request.method == 'POST' and 'voto' in request.POST:
            try:
                vote = Voto.objects.get(palabra=word, usuario=Usuario.objects.get(usuario=request.user))
                if vote.votado:
                    voto = 'Votar palabra'
                    vote.votado = False
                else:
                    voto = 'Quitar voto'
                    vote.votado = True
            except Voto.DoesNotExist:
                voto = 'Quitar voto'
                vote = Voto()
                vote.votado = True
                vote.palabra = p
                vote.usuario = Usuario.objects.get(usuario=request.user)
                vote.date = timezone.now()
            vote.save()
            p.contador = len(Voto.objects.filter(votado=True,palabra=word))
            p.save()
        else:
            try:
                vote = Voto.objects.get(palabra=word, usuario=Usuario.objects.get(usuario=request.user))
                if vote.votado:
                    voto = 'Quitar voto'
                else:
                    voto = 'Votar palabra'
            except Voto.DoesNotExist:
                voto = 'Votar palabra'
    except Word.DoesNotExist:
        voto=''
    return voto

def guardar_pal(request,word,defi):
    try:
        p = Word.objects.get(word=word)
        guarda = None
    except Word.DoesNotExist:
        if 'guarda' in request.POST:
            p = Word(word=word)
            p.defi = defi[0]
            p.img = defi[1]
            p.usuario = Usuario.objects.get(usuario=request.user)
            p.date = timezone.now()
            p.save()
            guarda = None
        else:
            guarda = 'Guardar palabra'
    return guarda


def acceder(request):
    data = request.POST
    if request.method == "POST":
        auth = AuthenticationForm(request, data)
        if auth.is_valid():
            username = auth.cleaned_data.get('username')
            password = auth.cleaned_data.get('password')
            user = authenticate(username=username, password=password)
            if user is not None:
                login(request, user)
                return HttpResponseRedirect('/')
            else:
                auth = AuthenticationForm()
        else:
            auth = AuthenticationForm()
        message = 'Nombre de usuario o contraseña inválidos'
    else:
        auth = AuthenticationForm()
        message = ''
    lista = Word.objects.all().order_by('-date')[0:5]
    listvoto = Word.objects.all().order_by('-contador')[0:10]
    try:
        palabra = [len(lista), random.choice(lista).word]
    except IndexError:
        palabra = [0, None]

    context = {'message': message, 'login': auth, 'listvoto': listvoto,
               'numpalabras': palabra[0], 'palabra': palabra[1]}
    return HttpResponse(render(request, 'registration/login.html', context))

def user_logout(request):
    logout(request)
    return HttpResponseRedirect("/")

def registro(request):
    if request.method == "POST":
        auth = UserForm(request.POST)
        if auth.is_valid():
            user = auth.save()
            user.set_password(user.password)
            user.save()
            u = Usuario(usuario=user)
            u.save()
            login(request, user)
            return HttpResponseRedirect('/')
        auth = UserForm()
        message = 'Nombre de usuario no válido o no disponible'
    else:
        auth = UserForm()
        message =''
    lista = Word.objects.all().order_by('-date')[0:5]
    lista_pal_voto = Word.objects.all().order_by('-contador')[0:10]
    try:
        palabra = [len(lista), random.choice(lista).word]
    except IndexError:
        palabra = [0, None]
    paginator = Paginator(lista, 5)

    num_pag = request.GET.get('page')
    objeto_pag = paginator.get_page(num_pag)
    context = {'register_form':auth, 'message': message, 'lista_pal_voto': lista_pal_voto,
               'numeropal': palabra[0], 'palabra': palabra[1], 'Lista': lista, 'objeto_pag': objeto_pag}
    return HttpResponse(render(request, 'pages/registro.html', context))



def mipagina(request):
    if request.method == 'GET':
        lista = Word.objects.all().order_by('-date')[0:5]
        lista_pal_voto = Word.objects.all().order_by('-contador')[0:10]
        try:
            palabra = [len(lista), random.choice(lista).word]
        except IndexError:
            palabra = [0, None]
        try:
            authform = AuthenticationForm()
            user = Usuario.objects.get(usuario=request.user)
            todo = []
            for item in Word.objects.filter(usuario=user).order_by('-date'):
                todo.append((item,item.date))
            for item in Comment.objects.filter(usuario=user).order_by('-date'):
                todo.append((item,item.date))
            for item in Info.objects.filter(usuario=user).order_by('-date'):
                todo.append((item,item.date))
            for item in Enlace.objects.filter(usuario=user).order_by('-date'):
                todo.append((item,item.date))
            todo.sort(key=fecha,reverse=True)
            final = []
            for item in todo:
                if (type(item[0])) == type(Word()):
                    final.append((item[0],'1'))
                elif type(item[0]) == type(Comment):
                    final.append((item[0],'2'))
                elif type(item[0]) == type(Info()):
                    final.append((item[0],'3'))
                elif type(item[0]) == type(Enlace()):
                    final.append((item[0],'4'))
            return HttpResponse(render(request, 'pages/mipagina.html',
                {'login': authform,
                'orden': final,
                'numeropal': palabra[0],
                'palabra': palabra[1],
                'lista_pal_voto': lista_pal_voto}))
        except TypeError:
            authform = AuthenticationForm()
            lista = Word.objects.all().order_by('-date')[0:5]
            lista_pal_voto = Word.objects.all().order_by('-contador')[0:10]
            try:
                palabra = [len(lista), random.choice(lista).word]
            except IndexError:
                palabra = [0, None]
            mensaje = 'Inicie sesión'
            return HttpResponse(render(request, 'pages/mipagina.html',
                {'login': authform,
                'mensaje': mensaje,
                'numeropal': palabra[0],
                'palabra': palabra[1],
                'lista_pal_voto': lista_pal_voto}))

def fecha(elemento):
    return elemento[1]

def ayuda(request):
    meth =  request.method
    if meth == 'GET':
        lista = Word.objects.all().order_by('-date')
        lista_pal_voto = Word.objects.all().order_by('-contador')[0:10]
        try:
            palabra = [len(lista), random.choice(lista).word]
        except IndexError:
            palabra = [0, None]
        authform = AuthenticationForm()
        form = request.POST.get('content')
        if form is not None:
            return HttpResponseRedirect('/'+str(form))
        else:
            response = render(request, 'pages/ayuda.html',
                {'login': authform,
                'lista_pal_voto': lista_pal_voto,
                'numeropal': palabra[0],
                'palabra': palabra[1]})
        return HttpResponse(response)
