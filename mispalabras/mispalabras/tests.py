from django.test import TestCase
from django.test import Client

class GetTests (TestCase):

    def test_getpals(self):

        c = Client()
        response = c.get('/ayuda/')
        self.assertEqual(response.status_code, 200)
        content = response.content.decode('utf-8')
        self.assertIn('<div id ="palprincipal">Ayuda</div>', content)

    def test_princ(self):
        c = Client()
        response = c.get('/')
        self.assertEqual(response.status_code, 200)
        content = response.content.decode('utf-8')
        self.assertIn('<div id ="cabecera">Mis Palabras</div>', content)
