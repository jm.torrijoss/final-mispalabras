from django import forms
from django.contrib.auth.models import User

class ContentForm(forms.Form):
    content = forms.CharField(label=False, widget=forms.Textarea(attrs={'rows':'2', 'cols': '105'}), max_length=50)

class UserForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ('username', 'password')# 'is_staff', 'is_superuser')#email', 'password')
        help_texts = {
            'username': None
        }
        widgets = {
           'password': forms.PasswordInput()
        }

class TextForm(forms.Form):
    choices = [(1,'Error 404'),(2,'Fim De Semana'),(3,'Frodo')]
    option = forms.ChoiceField(label=False,choices=choices)
    text = forms.CharField(label=False, widget=forms.Textarea(attrs={'rows':'1'}), max_length=50)

class UrlForm(forms.Form):
    enlace = forms.URLField(label=False, widget=forms.URLInput(attrs={'rows':'1'}), max_length=256)
