from xml.dom.minidom import parse
import urllib.request
from urllib.parse import quote
import json

def wiki(request, word):
    try:
        texto = "https://es.wikipedia.org/w/api.php?action=query&format=xml&titles="+str(quote(word))+"&prop=extracts&exintro&explaintext"
        xml = parse(urllib.request.urlopen(texto))
        extract = (xml.getElementsByTagName('extract')).item(0)
        defi = extract.firstChild.nodeValue
    except AttributeError:
        defi = ('No se ha encontrado definición')

    try:
        imagen = "https://es.wikipedia.org/w/api.php?action=query&titles="+str(quote(word))+"&prop=pageimages&format=json&pithumbsize=200"
        json_str = urllib.request.urlopen(imagen)
        json_doc = json_str.read().decode(encoding="ISO-8859-1")
        img = json.loads(json_doc)
        for key in img['query']['pages'].keys():
            id = key
        url_imagen = img['query']['pages'][id]['thumbnail']['source']
        #open_img = urlretrieve(url_imagen,'mispalabras/templates/pages/imagen.jpg')
    except KeyError:
        url_imagen = ''
    return (defi,url_imagen)
