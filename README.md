# Entrega practica
## Datos
* Nombre: José Manuel Torrijos Sánchez
* Titulación: Grado en Ingeniería en Sistemas de Telecomunicación
* Despliegue (url): http://jmtorrijoss.pythonanywhere.com
* Video básico (url): https://youtu.be/H_pNU2k4HUM
* Video parte opcional (url): https://youtu.be/kC3B-hnv-jA

 
## Cuenta Admin Site
* admin1/admin1
## Cuentas usuarios
* jose/jose
* 1234/1234
* tu/tu
* yo/yo
* 

## Resumen parte obligatoria
Realización de lo especificado en la práctica, registro de usuarios, logeo de los mismos, búsqueda de palabras, obtención de definiciones en wikipedia, rae, imágenes, etc

## Lista partes opcionales
* He añadido el favicon
* Contenidos XML y JSON de las palabras guardadas

## Problemas
* En el despliegue no carga bien el archivo css, ni imágenes, ni favicon
* Problemas con la definición de la RAE
* Problemas con los enlaces que se quieren guardar con una palabra

